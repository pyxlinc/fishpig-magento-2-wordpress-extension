<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Model;


class Homepage extends \FishPig\WordPress\Model\Homepage
{

	/**
	 * @return string
	 */
	public function getName()
	{
		if ($blogPage = $this->_getBlogPage()) {
			return $blogPage->getName();
		}
		return $this->_viewHelper->getBlogName();
	}

}