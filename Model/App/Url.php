<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Model\App;


class Url extends \FishPig\WordPress\Model\App\Url
{

	/**
	 * Rewriting this to avoid bug with any url of /{numbers}
	 * loading blog landing page with parameter of p={numbers}
	 *
	 * @param \Magento\Framework\App\RequestInterface $request
	 *
	 * @return array|bool|string
	 */
	public function getUrlAlias(\Magento\Framework\App\RequestInterface $request)
	{
		$pathInfo = $this->getPathInfo($request);
		$blogRoute = $this->getBlogRoute();

		if ($blogRoute && strpos($pathInfo, $blogRoute) !== 0) {
			return false;
		}

		if (trim(substr($pathInfo, strlen($blogRoute)), '/') === '') {
			return $pathInfo;
		}

		$pathInfo = explode('/', $pathInfo);

		// Clean off pager and feed parts
		if (($key = array_search('page', $pathInfo)) !== false) {
			if (isset($pathInfo[($key+1)]) && preg_match("/[0-9]{1,}/", $pathInfo[($key+1)])) {
				$request->setParam('page', $pathInfo[($key+1)]);
				unset($pathInfo[($key+1)]);
				unset($pathInfo[$key]);

				$pathInfo = array_values($pathInfo);
			}
		}

		/*
		// Clean off feed and trackback variable
		if (($key = array_search('feed', $pathInfo)) !== false) {
			unset($pathInfo[$key]);

			if (isset($pathInfo[$key+1])) {
				unset($pathInfo[$key+1]);
			}

			$request->setParam('feed', 'rss2');
			$request->setParam('feed_type', 'rss2');
		}
		*/

		// Remove comments pager variable
		foreach($pathInfo as $i => $part) {
			$results = array();
			if (preg_match("/" . sprintf('^comment-page-%s$', '([0-9]{1,})') . "/", $part, $results)) {
				if (isset($results[1])) {
					unset($pathInfo[$i]);
				}
			}
		}

		// removed this as it makes any number url go to blog landing page
//		if (count($pathInfo) == 1 && preg_match("/^[0-9]{1,8}$/", $pathInfo[0])) {
//			$request->setParam('p', $pathInfo[0]);
//
//			array_shift($pathInfo);
//		}

		$uri = urldecode(implode('/', $pathInfo));

		return $uri;
	}

}