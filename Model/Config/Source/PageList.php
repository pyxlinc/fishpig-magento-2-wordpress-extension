<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Model\Config\Source;


class PageList extends PostList
{

	/**
	 * Options Getter
	 *
	 * @param bool $showEmpty
	 *
	 * @return array
	 */
	public function toOptionArray($showEmpty = true)
	{
		return parent::toOptionArray('page', $showEmpty);
	}

}