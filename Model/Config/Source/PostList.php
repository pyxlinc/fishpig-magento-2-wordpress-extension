<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Model\Config\Source;


class PostList implements \Magento\Framework\Option\ArrayInterface
{

	/**
	 * @var \FishPig\WordPress\Model\ResourceModel\Post\CollectionFactory
	 */
	protected $_postCollectionFactory;

	/**
	 * PostList constructor.
	 *
	 * @param \FishPig\WordPress\Model\ResourceModel\Post\CollectionFactory $postCollectionFactory
	 */
	public function __construct(
		\FishPig\WordPress\Model\ResourceModel\Post\CollectionFactory $postCollectionFactory
	) {
		$this->_postCollectionFactory = $postCollectionFactory;
	}

	/**
	 * Options Getter
	 *
	 * @param string $postType
	 * @param bool $showEmpty
	 *
	 * @return array
	 */
	public function toOptionArray($postType = 'post', $showEmpty = true)
	{
		$arr = [];
		/** @var \FishPig\WordPress\Model\ResourceModel\Post\Collection $collection */
		$collection = $this->_postCollectionFactory->create();
		$collection
			->addPostTypeFilter($postType)
			->addIsPublishedFilter();

		/** @var \FishPig\WordPress\Model\Post $post */
		foreach ( $collection as $post ) {
			$arr[] = [
				'label' => $post->getName(),
				'value' => $post->getUrl()
			];
		}

		if($showEmpty){
			array_unshift($arr, array(
				'value' => '',
				'label' => ' ',
			));
		}
		return $arr;
	}

}