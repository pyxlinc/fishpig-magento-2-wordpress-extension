<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Shortcode;

/**
 * Class Year
 * @package Pyxl\WordPress\Shortcode
 *
 * Basic shortcode to inject the current year into content.
 */
class Year extends \FishPig\WordPress\Shortcode\AbstractShortcode
{
    /**
     *
     *
     * @return string
     **/
    public function getTag()
    {
        return 'year';
    }

    /**
     * Replace [year] shortcode with current year.
     *
     * @return $this
     **/
    protected function _process()
    {
        if (($shortcodes = $this->_getShortcodesByTag($this->getTag())) !== false) {
            foreach($shortcodes as $it => $shortcode) {
                $this->setValue(str_replace($shortcode['html'], date('Y'), $this->getValue()));
            }
        }

        return $this;
    }
}
