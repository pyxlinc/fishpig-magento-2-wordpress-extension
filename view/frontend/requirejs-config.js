var config = {
    map: {
        "*": {
            'infinitescroll': 'Pyxl_WordPress/js/infinitescroll',
        }
    },
    shim: {
        'Pyxl_WordPress/js/infinitescroll': {
            'deps': ['jquery']
        },
        'infinitescroll': {
            'deps': ['jquery']
        }
    }
};