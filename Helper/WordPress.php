<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Helper;


use Magento\Framework\App\Helper\Context;

class WordPress extends \Magento\Framework\App\Helper\AbstractHelper
{

	/**
	 * @var \FishPig\WordPress\Model\App
	 */
	protected $_app;

	/**
	 * @var \FishPig\WordPress\Model\App\Url
	 */
	protected $_wpUrlBuilder;

	/**
	 * @var \FishPig\WordPress\Model\App\Factory
	 */
	protected $_factory;

	/**
	 * @var \FishPig\WordPress\Model\Homepage
	 */
	protected $homepageModel;

	/**
	 * WordPress constructor.
	 *
	 * @param Context $context
	 * @param \FishPig\WordPress\Model\Context $wpContext
	 */
	public function __construct(
		Context $context,
		\FishPig\WordPress\Model\Context $wpContext
	) {
		$this->_app = $wpContext->getApp();
		$this->_wpUrlBuilder = $wpContext->getUrlBuilder();
		$this->_factory = $wpContext->getFactory();
		parent::__construct( $context );
	}

	/**
	 * Gets full url for blog listing page
	 *
	 * @return string
	 */
	public function getBlogListingUrl() {
		if ($this->homepageModel === null) {
			$this->homepageModel = $this->_factory->getFactory('Homepage')->create();
		}
		return $this->homepageModel->getUrl();
	}

}