<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Helper;


class InfiniteScroll extends \Magento\Framework\App\Helper\AbstractHelper
{

	/** @var \Magento\Store\Model\StoreManagerInterface */
	protected $_storeManager;

	/**
	 * @var \Magento\Cms\Model\Template\FilterProvider
	 */
	protected $_filterProvider;


	const XML_PATH_GENERAL_ENABLED = 'wordpress_infinitescroll/general/enabled';


	/**
	 * @param \Magento\Framework\App\Helper\Context      $context
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
	 */
	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Cms\Model\Template\FilterProvider $filterProvider
	) {
		parent::__construct($context);
		$this->_storeManager   = $storeManager;
		$this->_filterProvider = $filterProvider;
	}

	/**
	 * Return brand config value by key and store
	 *
	 * @param string $field
	 * @param string $default
	 * @param \Magento\Store\Model\Store|int|string $store
	 * @return string|null
	 */
	public function getConfig($field, $default="", $store = null)
	{
		$store = $this->_storeManager->getStore($store);

		$result = $this->scopeConfig->getValue(
			$field,
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$store);
		if($result == "") {
			$result = $default;
		}
		return $result;
	}

	/**
	 * @param string $field
	 * @param \Magento\Store\Model\Store|int|string $store
	 *
	 * @return string|null
	 */
	public function getConfigData($field, $store = null)
	{
		$store = $this->_storeManager->getStore($store);

		$result = $this->scopeConfig->getValue(
			$field,
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$store);
		return $result;
	}

	/**
	 * @param string $str
	 *
	 * @return string
	 */
	public function filter($str)
	{
		$html = $this->_filterProvider->getPageFilter()->filter($str);
		return $html;
	}

	/**
	 * Check if infinite scroll is enabled
	 *
	 * @param int|null $storeId
	 * @return null|string
	 */
	public function isEnabled($storeId = null)
	{
		return $this->getConfigData(self::XML_PATH_GENERAL_ENABLED, $storeId);
	}


	/**
	 * Escape quotes in javascript
	 *
	 * @param mixed $data
	 * @param string $quote
	 * @return mixed
	 */
	public function jsQuoteEscape($data, $quote='\'')
	{
		if (is_array($data)) {
			$result = array();
			foreach ($data as $item) {
				$result[] = str_replace($quote, '\\'.$quote, $item);
			}
			return $result;
		}
		return str_replace($quote, '\\'.$quote, $data);
	}


}