<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Block;

class Options extends \FishPig\WordPress\Block\AbstractBlock
{
    /**
     * @var \FishPig\Wordpress_ACF\Helper\Data
     */
    public $_acfHelper;

    /**
     * @var \FishPig\WordPress\Helper\Filter
     */
    public $_filter;

    /**
     * Options constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \FishPig\WordPress\Block\Context $wpContext
     * @param \FishPig\WordPress_ACF\Helper\Data $acfHelper
     * @param \FishPig\WordPress\Helper\Filter $filter
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \FishPig\WordPress\Block\Context $wpContext,
        \FishPig\WordPress_ACF\Helper\Data $acfHelper,
        \FishPig\WordPress\Helper\Filter $filter,
        array $data = []
    )
    {
        $this->_acfHelper = $acfHelper;
        $this->_filter = $filter;
        parent::__construct($context, $wpContext, $data);
    }

    /**
     * Filters ACF options field to parse shortcodes
     *
     * @param $field string
     * @return string
     */
    public function getFilteredOptionsField($field) {
        return $this->_filter->process(
            $this->_acfHelper->getOptionsField($field),
            $this
        );
    }

}
