<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Block;


class InfiniteScroll extends \Magento\Framework\View\Element\Template
{

	/**
	 * @var \Magento\Framework\Registry|null
	 */
	protected $coreRegistry = null;

	/**
	 * @var \Pyxl\WordPress\Helper\InfiniteScroll
	 */
	protected $helper;

	/**
	 * @var \Magento\Framework\Json\Helper\Data
	 */
	protected $jsonHelper;

	/**
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \Pyxl\WordPress\Helper\InfiniteScroll            $helper
	 * @param \Magento\Framework\Registry                      $registry
	 * @param \Magento\Framework\Json\Helper\Data              $jsonHelper
	 * @param array                                            $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Pyxl\WordPress\Helper\InfiniteScroll $helper,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Json\Helper\Data $jsonHelper,
		array $data = []
	) {
		parent::__construct($context, $data);
		$this->helper    = $helper;
		$this->coreRegistry = $registry;
		$this->jsonHelper = $jsonHelper;
	}

	/**
	 * @return \Pyxl\WordPress\Helper\InfiniteScroll
	 */
	public function getHelper() {
		return $this->helper;
	}

	/**
	 * Check if infinite scroll is enabled for current view
	 *
	 * @return bool
	 */
	public function isEnable() {
		return $this->helper->isEnabled();
	}

	/**
	 * Get loader image from config
	 *
	 * @return bool|string
	 */
	public function getLoaderImage()
	{

		$url = $this->helper->getConfig('wordpress_infinitescroll/design/loading_image');
		if(!empty($url)) {
			$url = strpos($url, 'http') === 0 ? $url : $this->getViewFileUrl($url);
		}
		return empty($url) ? false : $url;
	}

	/**
	 * @return string
	 */
	public function getIasConfig() {
		$data = array(
			'item' => $this->helper->getConfig('wordpress_infinitescroll/selectors/items'),
			'container' => $this->helper->getConfig('wordpress_infinitescroll/selectors/content'),
			'next' => $this->helper->getConfig('wordpress_infinitescroll/selectors/next'),
			'pagination' => $this->helper->getConfig('wordpress_infinitescroll/selectors/pagination'),
			'parameter' => (bool) $this->helper->getConfig('wordpress_infinitescroll/instances/enable_parameter'),
			'delay' => 600,
			'negativeMargin' => (int) $this->helper->getConfig('wordpress_infinitescroll/design/buffer'),
			'history' => array(
				'prev' => $this->helper->getConfig('wordpress_infinitescroll/selectors/previous')
			),
			'noneleft' => array(
				'text' => $this->helper->jsQuoteEscape(
					__($this->helper->getConfigData('wordpress_infinitescroll/design/done_text'))
				),
				'html' => '<div class="ias-noneleft" style="text-align: center;">{text}</div>'
			),
			'spinner' => array(
				'html' => '<div class="ias-spinner col-12" style="text-align: center;">'
				          .'<img style="display:inline" src="{src}"/>'
				          .$this->helper->jsQuoteEscape(
						__($this->helper->getConfigData('wordpress_infinitescroll/design/loading_text'))
					)
				          .'</div>'
			),
			'trigger' => array(
				'text' => $this->helper->jsQuoteEscape(
					__($this->helper->getConfigData('wordpress_infinitescroll/design/load_more_text'))
				),
				'html' => $this->getTriggerHtml('next'),
				'textPrev' => $this->helper->jsQuoteEscape(
					__($this->helper->getConfigData('wordpress_infinitescroll/design/load_more_text'))
				),
				'htmlPrev' => $this->getTriggerHtml('prev'),
				'offset' => (int) $this->helper->getConfigData('wordpress_infinitescroll/design/load_more'),
				'showOnce' => (bool) $this->helper->getConfigData('wordpress_infinitescroll/design/show_trigger_once')
			)
		);
		if ($spinner = $this->getLoaderImage()) {
			$data['spinner']['src'] = $spinner;
		}
		return $this->jsonHelper->jsonEncode($data);
	}

	/**
	 * @param string $dir
	 *
	 * @return string
	 */
	protected function getTriggerHtml( $dir = 'next' ) {
		return <<<HTML
<div class="ias-trigger ias-trigger-{$dir} text-center col-12">
	<div class="row justify-content-center">
		<button class="btn-dark col col-md-6 col-lg-4">{text}</button>	
	</div>
</div>
HTML;
	}

}
