<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Block;


class FlexibleContent extends \FishPig\WordPress\Block\Post
{

	/**
	 * Renders flexible content for give layout
	 * Expects children blocks to be created in layout
	 *
	 * @param array $data
	 *
	 * @return null|string
	 */
	public function renderContent( $data ) {
		if ($block = $this->getChildBlock($data['acf_fc_layout'])) {
			$block->setData('flexible_content', $data);
			return $block->toHtml();
		}
		return null;
	}

}