<?php
/**
 * @category    Pyxl
 * @package     Pyxl_WordPress
 * @copyright   2017 Joel Rainwater
 * @license     http://opensource.org/licenses/mit-license.php MIT License
 * @author      Joel Rainwater <jrainwater@thinkpyxl.com>
 */

namespace Pyxl\WordPress\Block\Catalog\Product;


class Listing extends \Magento\Catalog\Block\Product\ListProduct
{

}