# Magento 2 WordPress Extension
This extends the functionality of the FishPig Magento 2 WordPress integration and their ACF add-on.

## Requirements
In order for this to work you must have <a href="https://fishpig.co.uk/magento-2/wordpress-integration/" target="_blank">FishPig_WordPress</a> 
and <a href="https://fishpig.co.uk/magento-2/wordpress-integration/advanced-custom-fields/" target="_blank">FishPig_WordPress_ACF</a> modules installed and enabled.   

## Getting Started
To install this module run the following. 

    composer config repositories.pyxl-wordpress git git@bitbucket.org:pyxlinc/fishpig-magento-2-wordpress-extension.git
    composer require pyxl/wordpress:dev-master
    bin/magento module:enable Pyxl_WordPress
    bin/magento setup:upgrade
    bin/magento cache:clean
    

## Features
List of features that this extension provides or enhances. 

#### Options Page Fields
A new block type is created that provides a simple way to display content from an Options Page ACF field. 
Includes the ability to filter shortcodes. 

**Block Type** `\Pyxl\WordPress\Block\Options` 

To use this block, first insert a new block of this type in your layout. You will need to create a template in your 
theme and reference it in the layout. 

    <block class="Pyxl\WordPress\Block\Options" name="options.block" template="Magento_Theme::path/to/template.phtml"/>
    
In your template file you can use the following calls:

    /** @var $_acfHelper \FishPig\WordPress_ACF\Helper\Data */
    $_acfHelper = $block->_acfHelper;
    $_acfHelper->getOptionsField('options_field_name');
    
In order to account for shortcodes in the field value you can instead call this:

    $block->getFilteredOptionsField('options_field_name');
    
#### Shortcodes
FishPig's integration comes with some shortcodes but this extension provides additional options. 

##### [product] 
This is currently in FishPig's road map but we don't have an expected date yet. In the meantime I have 
created a temporary shortcode to allow products to be inserted into WordPress pages/posts.
The templates are located at `/view/frontend/templates/shortcode/product.phtml` and 
`/view/frontend/templates/shortcode/product-listing.phtml`. 

    [product id="1"] // Single product by ID.
    [product ids="1,2,3"] // Listings of products by IDs.  
    [product sku="my-sku"] // Single product by SKU.
    [product skus="sku1,sku2,sku3"] // Listing of products by SKUs.

##### [year]
This is a very simple shortcode that echos the current year. Generally used for Copyright section.
Usage is just `[year]`.

#### Infinite Scroll
To enable and configure infinite scroll for the blog listing page navigate to 
Stores -> Configuration -> FishPig -> WordPress Infinite Scroll. This is for use on the blog listing page and 
term pages (category view). 

## Authors
* Joel Rainwater
